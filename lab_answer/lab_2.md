#### Favian Naufal (2006597802)
Pertanyaan Singkat Lab 2

## Soal 1
1. Apakah perbedaan antara JSON dan XML?

### Jawab
JSON atau JavaScript Object Nation adalah format pertukaran data komputer dan memiliki JavaScript sebagai dasar bahasa pemrogramman. Sedangkan XML atau Extensible Markup Language adalah markup language yang menerapkan
peraturan-peraturan dalam melakukan encoding document, dengan format yang dapat di baca (readable) oleh manusia dan mesin (machine-readable). Perbedaan lainnya berupa:
* JSON tidak mendukung penggunaan namespaces, sedangkan XML mendukung penggunaan namespaces
* JSON mendukung Array, sedangkan XML tidak mendukung array
* JSON tidak memerlukan tag penutup, sedangkan XML memerlukan tag penutup, dan lain-lain.

## Soal 2
2. Apakah perbedaan antara HTML dan XML?

### jawab
XML memiliki fungsi yang berbeda dengan HTML. Dimana XML memiliki fungsi untuk menyimpan dan mengirimkan data,
dibandingkan HTML yang hanya berfungsi untuk menampilkan data. Struktur pada XML juga berbeda dengan HTML, dimana struktur XML terdiri dari tiga bagian, yaitu bagian Deklarasi, Atribut, dan Elemen. Perbedaan lain yang cukup signifikan adalah
sebagai berikut: 
* XML bersifat dynamic, sedangkan HTML bersifat static
* XML bersifat case sensitive, sedangkan HMTL tidak bersifat case sensitive
* XML memerlukan tag penutup didalamnya, sedangkan dalam HTML tidak diperlukan, dan lain-lain.
