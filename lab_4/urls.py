from django.urls import include, path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add_note', add_note, name='add'),
    path('note-list', note_list, name='note-list'),
]